const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const browserSync = require('browser-sync').create();

// locations
const css_location = './src/assets/css';
const scss_location = './src/assets/scss/**/*.scss';
const html_location = './src/*.html';
const js_location = './src/assets/scripts/**/*.js';
const server_location = './src/';

// compile to css
const style = () => (
    gulp.src(scss_location)
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(gulp.dest(css_location))
    .pipe(browserSync.stream())
);

const watch = () => {
    browserSync.init({
        server: {
            baseDir: server_location
        }
    });
    gulp.watch(scss_location, style);
    gulp.watch(html_location).on('change', browserSync.reload);
    gulp.watch(js_location).on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;